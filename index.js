	//Index.js serves as the server
	var express = require('express');
	var app = express();
	var http = require('http').Server(app);
	var io = require('socket.io')(http);
	var dmpfile = require('diff_match_patch');
	//adding mysql 
	var mysql      = require('mysql');
	var connection = mysql.createConnection({
	host     : 	process.env.OPENSHIFT_MYSQL_DB_HOST,
	user     : 	'admineiAI5S2',
	password : 'RI1yMb7qNjRx',
	database : 'scrapr'
	});

	//creating connection to database
	connection.connect();

	//to access static files
	app.use('/static', express.static(__dirname + '/public'));
	
	
	//when it gets index page request
	app.get('/', function(req, res)
	{
	  res.sendFile(__dirname + '/index.html');
	});

	//when it gets doc page request
	app.get('/doc/:docname', function(req, res)
	{
	  res.sendFile(__dirname+'/doc.html');
	});

	console.log(process.env.OPENSHIFT_MYSQL_DB_HOST);
	//to access the static file
	app.use(express.static(__dirname + '/public'));

	//to store data on server side
	var docs=[];	

	//to get doc parameters from all the docs
	function find(name)
	{
	  var length=docs.length;
	  for(i=0;i<length;i++)
	  {
		if(docs[i].docname==name)
		{
		  return i;
		}
	  }
	  return -2;
	}

	//initiating connection and socket
	
	io.sockets.on('connection', function(socket) 
	{	
		//creating a new doc
	  socket.on('create', function(data) 
	  {       
		//checking for existing doc with same name
		query='SELECT * from `docdata` where docname = "' +data.docname+'"';
		
		connection.query(query, function(err, rows, fields)
		{   
			//if no error in query proceed
			if (!err) 
			{	
				//if no doc exists with the doc name tried to create
				if(rows.length==0)
					{	
						var id=docs.push(data);
						id=id-1;
						docs[id].docid=id;
						//socket.join(data.docname);
						query='Insert into `docdata` (docname) values ("' +data.docname+ '")'; 
						connection.query(query, function(err, rows, fields)
						{
							if (!err)
							{
								console.log("entry updated");
							}									
						});
						socket.emit('createfeedback' , data);
					}
					else
					{
						socket.emit('createfeedback' , 0);
						
					}
			}
			else
			{
				socket.emit('createfeedback' , 404);
			}
		});
		
	});	
	 //create ends
	
	//joining a doc
	socket.on('join', function(data)
	{
	
		var n=find(data.docname);
		
		if(n==-2)
		{	
			query='SELECT * from `docdata` where docname = "' +data.docname+'"';
	
			connection.query(query, function(err, rows, fields)
			{
				
				if(rows.length==0)
				{
					socket.emit('joinerror',0);
				}
				else
				{	
					var id=docs.push(data);
					id=id-1;
					docs[id].docid=id;
					docs[id].doctext=rows[0].doctext;
					console.log(rows[0].doctext);
					docs[id].numberofusers+=1;
					socket.join(data.docname);
					io.sockets.in(data.docname).emit('anewuserjoined', docs[id]);
				}
			});

		}
		else
		{
		id=find(data.docname);
		socket.join(data.docname);
		docs[id].users.push(data.users[0]);
		docs[id].doctext=docs[n].doctext+"";
		docs[id].numberofusers+=1;
		console.log(docs[id].numberofusers);
		io.sockets.in(data.docname).emit('anewuserjoined', docs[id]);
		}
		
		
		
		
		
		
		
	}); //join ends
	
	//when request comes to update all docs on joining of a new user
	
	
	//updating the database
	socket.on('updatedatabase', function(data)
	{	
		query='update `docdata` set doctext = "' +data.doctext+ '" where docname = "' +data.docname+'"';
		
		
		connection.query(query, function(err, rows, fields)
		{
			if (!err)
				{
					socket.to(data.docname).emit('databaseupdated');
				}	
			else
			{
				console.log(err);
			}
		});
		
	});//update database ends
	
	//sending edits to all but the sender
	socket.on('texttype', function(data)
	{	
		var dmp = new dmpfile.diff_match_patch();
		
		var ds = dmp.diff_textsub(data.diff);
		
		docs[data.docid].doctext=ds;
		socket.broadcast.to(data.docname).emit('texttype', data);
	});
	
	});
	
	var port =  process.env.OPENSHIFT_NODEJS_PORT || 8080;   // Port 8080 if you run locally
	var address =  process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1"; // Listening to localhost if you run locally
	http.listen(port, address);
	